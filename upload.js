var config = require('./config');
var fs = require('fs');
var redis = require('./redisSingleTon');
var imageSizes = require('image-size');
var nodeName = config.get('node_name');
var AWS = require('aws-sdk');
var s3 = new AWS.S3({accessKeyId: config.get('aws:s3:id'), secretAccessKey: config.get('aws:s3:secret'), region:config.get("aws:s3:region")});
var request = require('request');

var debug = true;

var specialCases = {
    // '53,300':'53,300',
    // '53,310':'53,300',
    '76,260':'76,260', //DONE
    // '50,250':'50,250',
    '57,300':'57,300', //DONE
    '58,294':'57,300', //DONE
    '45,122':'45,122', //DONE
    '80,230':'45,122', //DONE
    '40,206':'40,206',
    // '50,200': 'second',
    // '50,130': 'second',
    // '50,180': 'second',
    // '60,200': 'second',
    // '70,200': 'second'
};
var bucket = config.get('aws:s3:bucket');

var exit = false;
function blockForJob() {
    if (exit)
        return;
    redis.lpop(nodeName + ':prelist', function (err, userId_taskId) {
        if (err) {
            shutdownApp(err || 'emptyTaskId');
        } else if (userId_taskId) {
            if (debug)
                console.log('got task to upload', userId_taskId);
            var userId = userId_taskId.split('_')[0];
            var taskId = userId_taskId.split('_')[1];
            track(userId, 'task');

            fs.readFile('/tmp/'+userId_taskId, function (err, buffer) {
                var queue = 'tasks';
                var errorCode = null;
                var errorMessage = null;
                fs.unlink('/tmp/'+userId_taskId, function () {});
                if (err || !buffer) {
                    errorCode = 15;
                    errorMessage = 'ERROR_IMAGE_TYPE_NOT_SUPPORTED';
                } else {
                    var sizes = imageSizes(buffer);
                    if (!sizes || !sizes.height || !sizes.width) {
                        errorCode = 15;
                        errorMessage = 'ERROR_IMAGE_TYPE_NOT_SUPPORTED';
                    } else  if (sizes.height > 400 || sizes.width > 400) {
                        errorCode = 4;
                        errorMessage = 'ERROR_TOO_BIG_CAPTCHA_FILESIZE';
                    } else if (sizes.height < 20 || sizes.width < 20) {
                        errorCode = 3;
                        errorMessage = 'ERROR_ZERO_CAPTCHA_FILESIZE';
                    } else {
                        var size = sizes.height+ ',' + sizes.width;
                        if (size in specialCases) {
                            queue = specialCases[size] + 'tasks';
                        }
                    }
                }
                if (errorCode && errorMessage) {
                    if (debug)
                        console.log(errorCode, errorMessage);

                    redis.hset('user:tasks:' + userId, taskId, JSON.stringify({errorId:errorCode, description:errorMessage, status:"ready", cost:"0", createTime: parseInt(Date.now()/1000), endTime:parseInt(Date.now()/1000)}), blockForJob);
                } else {
                    var params = {
                        Bucket: bucket, /* required */
                        Key: userId_taskId, /* required */
                        ACL: 'public-read',
                        Body: buffer,
                        CacheControl: 'max-age=31536000',
                        Expires: new Date(Date.now()+1000*60*60*13),
                        ContentType: 'image/jpeg',
                        StorageClass: 'REDUCED_REDUNDANCY'
                    };
                    s3.putObject(params, function(err, data) {
                        if (debug)
                            console.log('uploaded to s3', err);
                        if (err) {
                            console.log('err while put to s3', userId_taskId, err);
                            redis.hset('user:tasks:' + userId, taskId, JSON.stringify({errorId:15, description:'ERROR_IMAGE_TYPE_NOT_SUPPORTED', status:"ready", cost:"0", createTime: parseInt(Date.now()/1000), endTime:parseInt(Date.now()/1000)}), function () {});
                        } else {
                            redis.rpush(queue, userId_taskId, function (err) {
                                if (debug)
                                    console.log('pushed to queue', queue , err);
                                if (err) {
                                    console.log('Error to push', userId_taskId, 'to queue', queue, err);
                                }
                            });
                        }
                    });
                    blockForJob();
                }
            });
        } else {
            setTimeout(blockForJob, 100);
        }
    })
}

process.on('SIGINT', function() {
    exit = true;
    setTimeout(function () {
        process.exit(0);
    }, 5000);
});

function shutdownApp(err) {
    console.log('shutdown because');
    console.log(err);
    if (!exit) {
        exit = true;
        setTimeout(function () {
            process.exit(1);
        }, 5000);
    }
}
blockForJob();

function track(id ,event) {
    request('http://log.mnillstone.com/event/58cc02a555705d033788ea95/' + id + '/' + event + '/0?_' + Date.now() + Math.random(), function (err, data) {

    });
}