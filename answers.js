var config = require('./config');
var redis = require('./redisSingleTon');
var request = require('request');

var debug = false;
var exit = false;
var today = new Date();
today = new Date(+today - today.getMilliseconds() - today.getSeconds()*1000 - today.getMinutes()*60*1000  - today.getHours()*60*60*1000).getTime() + 1000*60*60*24;
setInterval(function () {
    today = new Date();
    today = new Date(+today - today.getMilliseconds() - today.getSeconds()*1000 - today.getMinutes()*60*1000  - today.getHours()*60*60*1000).getTime() + 1000*60*60*24;
}, 30000);

function blockForJob() {
    if (exit)
        return;
    redis.lpop('answers', function (err, userId_taskId_answer) {
        if (err) {
            shutdownApp(err || 'emptyTaskId');
        } else if (userId_taskId_answer) {
            if (debug)
                console.log('got answer', userId_taskId_answer);

            blockForJob();

            var userId = userId_taskId_answer.split('_')[0];
            var taskId = userId_taskId_answer.split('_')[1];
            var answer = userId_taskId_answer.split('_')[2];

            redis.hget('user:tasks:' + userId, taskId, function (err, data) {
                if (err) {
                    shutdownApp(err);
                } else if (data) {
                    try {
                        data = JSON.parse(data);
                        data.status = "ready";
                        data.solution = {
                            "text": answer
                        };
                        track(userId, 'answer');
                        redis.multi().hincrby('userUsage:' + today, userId, 1).expire('userUsage:' + today, 2764800).hincrby('solved', userId, 1).hincrby('balance', userId, -1).hset('user:tasks:' + userId, taskId, JSON.stringify(data)).exec(function () {});
                    } catch (e) {console.log(e);}
                }
            });
        } else {
            setTimeout(blockForJob, 100);
        }
    })
}

process.on('SIGINT', function() {
    exit = true;
    setTimeout(function () {
        process.exit(0);
    }, 5000);
});

function shutdownApp(err) {
    console.log('shutdown because');
    console.log(err);
    if (!exit) {
        exit = true;
        setTimeout(function () {
            process.exit(1);
        }, 5000);
    }
}
blockForJob();

function track(id ,event) {
    request('http://log.mnillstone.com/event/58cc02a555705d033788ea95/' + id + '/' + event + '/0?_' + Date.now() + Math.random(), function () {
        
    });
}