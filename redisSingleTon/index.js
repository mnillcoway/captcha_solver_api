/**
 * Created by ilya on 13.05.16.
 */

var redis = require("redis");
var config = require('../config');

var client = redis.createClient(config.get('redis:port'), config.get('redis:address'));
client.auth(config.get('redis:auth'));
module.exports = client;