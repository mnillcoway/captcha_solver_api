var express = require('express');
var config = require('./config');
var logger = require('morgan');
var Address6 = require('ip-address').Address6;
var bodyParser = require('body-parser');
var limiter = require('./utils/requestLimiter').limit;
var fs = require('fs');

var app = express();
var redis = require('./redisSingleTon');

var nodeName = config.get('node_name');
var HttpError = require('./error').HttpError,
    AppError = require('./error').AppError;

var exit = false;
process.on('SIGINT', function() {
    exit = true;
    setTimeout(function () {
        process.exit(0);
    }, 5000);
});

app.set('port', config.get("port"));
if (config.get('env') === 'production') {
    app.use(logger('combined', {
        skip: function (req, res) {
            return res.statusCode < 400
        }
    }));
} else {
    app.use(logger('dev'));
}

var debug = true;
app.use(function (req, res, next) {
    if (exit) {
        next(new AppError(2, "ERROR_NO_SLOT_AVAILABLE"));
    } else {
        var ip = (req.headers['x-forwarded-for'] || req.headers['x-ip'] || req.connection.remoteAddress).split(',')[0];
        if (ip && ip.indexOf(':') !== -1) {
            var add = new Address6(ip);
            if (add && add.valid) {
                ip = add.parsedAddress.slice(0, 4).join(':')
            }
        }
        req.ip = ip;
        redis.get('banned:' + req.ip, function (err, count) {
            count = count | 0;
            next(err || (count > 0 ? new HttpError(429, 'Too Many Requests') : null));
        })
    }
});

app.use(bodyParser.json({limit:'200kb'}));

var limiterForLogin = limiter('api_login', 30, 60);
app.use(function (req, res, next) {
    if (req.body && 'clientKey' in req.body && (typeof req.body.clientKey === 'string' || req.body.clientKey instanceof String)) {
        if (req.body.clientKey === config.get('admin_key')) {
            req.user = {id:0, balance:1000};
            return next();
        }
        redis.hget('id_by_api_key', req.body.clientKey, function (err, id) {
            if (err) {
                next(err);
            } else if (!id) {
                limiterForLogin(req, res, function (err) {
                    next(err || new AppError(1, 'ERROR_KEY_DOES_NOT_EXIST'));
                });
            } else {
                redis.hget('balance', id, function (err, balance) {
                    if (err || !balance || parseInt(balance) <= 0) {
                        next(err || new AppError(10, 'ERROR_ZERO_BALANCE'));
                    } else {
                        req.user = {id:id, balance:balance};
                        next();
                    }
                })
            }
        })
    } else {
        next(new AppError(1, 'ERROR_KEY_DOES_NOT_EXIST'));
    }
});

app.post('/createTask', function (req, res, next) {
    if (!('task' in req.body) || req.body.task.type !== 'ImageToTextTask') {
        // redis.multi().zadd('user:tasks_list:' + req.user.id, Date.now(), taskId).zadd('expire:tasks', Date.now(), req.user.id +'_' + taskId).hset('user:tasks:' + req.user.id, taskId, JSON.stringify({errorId:14, description:'ERROR_NO_SUCH_METHOD'})).exec(function (err) {
        //     if (err)
        //         next(err);
        //     else {
        res.json({errorId:14, description:'ERROR_NO_SUCH_METHOD'});
        //     }
        // })
    } else if (!(typeof req.body.task.body === 'string' || req.body.task.body instanceof String) || req.body.task.body.length < 100) {
        // redis.multi().zadd('user:tasks_list:' + req.user.id, Date.now(), taskId).zadd('expire:tasks', Date.now(), req.user.id +'_' + taskId).hset('user:tasks:' + req.user.id, taskId, JSON.stringify({errorId:3, description:'ERROR_ZERO_CAPTCHA_FILESIZE'})).exec(function (err) {
        //     if (err)
        //         next(err);
        //     else {
        res.json({errorId:3, description:'ERROR_ZERO_CAPTCHA_FILESIZE'});
        //     }
        // })
    } else {
        redis.hincrby('user:task_id', req.user.id, 1, function (err, taskId) {
            if (err || !taskId) {
                next(err || new HttpError(500));
            } else {
                try {
                    var decoded = decodeBase64Image(req.body.task.body);
                    fs.writeFile('/tmp/'+req.user.id +'_' + taskId, decoded.data, function (err) {
                        if (err) {
                            next(err);
                        } else {
                            redis.multi().zadd('user:tasks_list:' + req.user.id, Date.now(), taskId).zadd('expire:tasks', Date.now(), req.user.id +'_' + taskId).hset('user:tasks:' + req.user.id, taskId, JSON.stringify({errorId:0, status:"processing", cost:"0.0001", createTime:parseInt(Date.now()/1000), ip:req.ip})).rpush(nodeName + ':prelist', req.user.id + '_' + taskId).exec(function (err) {
                                if (err) {
                                    next(err);
                                } else {
                                    res.json({errorId:0, taskId:taskId});
                                }
                            })
                        }
                    });
                } catch (e) {
                    if (debug)
                        console.log(e);
                    return next(new AppError(15, 'ERROR_IMAGE_TYPE_NOT_SUPPORTED'))
                }
            }
        });
    }
});

app.post('/getTaskResult', function (req, res, next) {
    if (!('taskId' in req.body) || isNaN(req.body.taskId)) {
        res.json({errorId:14, description:'ERROR_NO_SUCH_METHOD'});
    } else {
        redis.hget('user:tasks:' + req.user.id, req.body.taskId, function (err, data) {
            if (err || !data) {
                next(err || new AppError(16, 'ERROR_NO_SUCH_CAPCHA_ID'));
            } else {
                try {
                    data = JSON.parse(data);
                    res.json(data);
                } catch (e) {
                    return next(e);
                }
            }
        })
    }
});

app.use(function(req, res, next) {
    var err = new HttpError(404, 'Not Found');
    next(err);
});

app.use(function(err, req, res, next) {
    var message,
        errorId = 500;
    if (err instanceof HttpError) {
        message = err.message;
        if (err.status === 429)
            errorId = err.status;
    } else if (err instanceof AppError) {
        message = err.message;
        errorId = err.errorId;
    } else {
        console.log(err);
        message = 'INTERNAL_ERROR';
    }
    res.status(200);
    res.json({errorId:errorId, description:message});
});

var server = app.listen(config.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

if (config.get('env') === 'production') {
    console.log("server has been restarted", new Date());
}

function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};

    if (!matches || matches.length !== 3) {
        matches = ['true', 'unknown', dataString];
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    if (response.type === 'image/jpeg' || response.type === 'image/jpg')
        response.type = 'jpg';
    else if (response.type === 'image/png')
        response.type = 'png';
    else if (response.type === 'image/gif')
        response.type = 'gif';

    return response;
}
