var nconf = require('nconf');
var path = require('path');

nconf.argv()
    .env()
    .file({ file: path.join(__dirname, process.env.NODE_ENV != 'test' ?  'config.json' : 'config-test.json') });

module.exports = nconf;