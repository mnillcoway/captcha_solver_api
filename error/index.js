var path = require('path');
var util = require('util');
var http = require('http');

// ошибки для выдачи посетителю
function HttpError(status, message) {
    if (this.constructor !== HttpError)
        return new HttpError(status, message);
    Error.apply(this, arguments);
    Error.captureStackTrace(this, HttpError);

    this.status = status;
    this.message =  message || http.STATUS_CODES[status] || "Error";
}

util.inherits(HttpError, Error);
HttpError.prototype.name = 'HttpError';
exports.HttpError = HttpError;


function AppError(errorId, message) {
    if (this.constructor !== AppError)
        return new AppError(status, message);
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AppError);

    this.status = 200;
    this.errorId = errorId || 500;
    this.message =  message || "INTERNAL_ERROR";
}

util.inherits(AppError, Error);
AppError.prototype.name = 'AppError';
exports.AppError = AppError;